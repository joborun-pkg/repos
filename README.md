## repos

on /etc/pacman.conf under each of the three joborun repositories
there should be this line:

    Include = /etc/pacman.d/mirrorlist-jobo

On this mirror file if for some reason sourceforge is not
working you can get the pacman databases for upgrades from here 
(disroot, which is always working):

    Server = https://git.disroot.org/joborun-pkg/repos/raw/branch/main/

When sourceforge is working the first uncommented line (mirror)
should be:

    Server = http://downloads.sourceforge.net/joborun/r

You may comment this out and select a specific one nearest to you.
The first uncommented mirror on the list is attempted first, if it fails
or does not respond, then the next and so on.

From either sf or disroot you can install the latest pacman and jobo-mirror,
then check your /etc/pacman.conf.pacnew and /etc/pacman.d/mirrorlist-jobo.pacnew
for the latest changes.  No other binary packages are offered through
disroot (space limitation), only pacman databases, pacman, and jobo-mirror pkg.

